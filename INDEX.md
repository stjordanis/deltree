# DELTREE

Delete files and directories with all included files and subdirectories!


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## DELTREE.LSM

<table>
<tr><td>title</td><td>DELTREE</td></tr>
<tr><td>version</td><td>1.02g.mrlg (rev A)</td></tr>
<tr><td>entered&nbsp;date</td><td>2013-03-30</td></tr>
<tr><td>description</td><td>Delete files and directories with all included files and subdirectories!</td></tr>
<tr><td>keywords</td><td>freedos, delete, tree</td></tr>
<tr><td>author</td><td>Charles Dye, cdye -at- unm.edu</td></tr>
<tr><td>maintained&nbsp;by</td><td>MR-LEGO, MR-LEGO.SW -at- web.de</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Deltree</td></tr>
</table>
